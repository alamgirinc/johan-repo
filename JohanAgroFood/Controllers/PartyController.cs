﻿using Johan.DATA;
using Johan.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JohanAgroFood.Controllers
{
    public class PartyController : Controller
    {
       private IPartyRepository partyRepository;

        //constructor
       public PartyController()
        {
            if (partyRepository == null)
            {
                this.partyRepository = new DPartyRepository(new JohanAgroFoodDBEntities());
            }
        }
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        public JsonResult SaveParty(tblParty party)
        {
            return Json(partyRepository.SaveParty(party),JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetParty()
        {
            var partyList = partyRepository.GetParty();
            return Json(partyList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult EditParty(tblParty objEditParty)
        {
            return Json(partyRepository.EditParty(objEditParty), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int pk)
        {
            return Json(partyRepository.Delete(pk),JsonRequestBehavior.AllowGet);
        }
    }
}