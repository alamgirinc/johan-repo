﻿using Johan.DATA;
using Johan.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JohanAgroFood.Controllers
{
    public class StockController : Controller
    {
        private IStockRepository stkRepository;

        //constructor
        public StockController()
        {
            if (stkRepository == null)
            {
                this.stkRepository = new DStockRepository(new JohanAgroFoodDBEntities());
            }
        }
        // GET: Login
        public ActionResult StockIndex()
        {
            return View();
        }
        public ActionResult ProductStock()
        {
            return View();
        }

        public JsonResult GetProdStocks()
        {
            var riceStkLst = stkRepository.GetProdStocks();
            return Json(riceStkLst, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveProdStock(STK_Balance prodStk)
        {
            return Json(stkRepository.SaveProdStock(prodStk), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteProdStock([System.Web.Http.FromBody] STK_Balance prodStk)
        {
            return Json(stkRepository.DeleteProdStock(prodStk), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditProdStock([System.Web.Http.FromBody] STK_Balance prodStk)
        {
            return Json(stkRepository.EditProdStock(prodStk), JsonRequestBehavior.AllowGet);
        }













        public JsonResult GetStock(STK_tblStock stk)
        {
            var stkList = stkRepository.GetStock(stk);
            return Json(stkList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveStock(STK_tblStock stk)
        {
            return Json(stkRepository.SaveStock(stk), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditStock([System.Web.Http.FromBody] STK_tblStock stk)
        {
            return Json(stkRepository.EditStock(stk), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteStock(int pk)
        {
            return Json(stkRepository.DeleteStock(pk), JsonRequestBehavior.AllowGet);
        }
    }
}