﻿using Johan.DATA;
using Johan.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JohanAgroFood.Controllers
{
    public class ConsumptionController : Controller
    {
         private IConsumptionRepository consumptionRepository;

        //constructor
         public ConsumptionController()
        {
            if (consumptionRepository == null)
            {
                this.consumptionRepository = new DConsumptionRepository(new JohanAgroFoodDBEntities());
            }
        }
        // GET: Consumption
        public ActionResult ConsumptionIndex()
        {
            return View();
        }

        [HttpPost]
        public JsonResult DeleteConsumption([System.Web.Http.FromBody] tblCostingSource consumption)
        {
            return Json(consumptionRepository.DeleteConsumption(consumption), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditConsumption([System.Web.Http.FromBody] tblCostingSource consumption)
        {
            return Json(consumptionRepository.EditConsumption(consumption), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveConsumption(tblCostingSource consumption)
        {
            return Json(consumptionRepository.SaveConsumption(consumption), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetConsumption()
        {
            var ConsumptionList = consumptionRepository.GetConsumption();
            return Json(ConsumptionList, JsonRequestBehavior.AllowGet);
        }
    }
}