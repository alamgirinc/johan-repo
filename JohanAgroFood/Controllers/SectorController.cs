﻿using Johan.DATA;
using Johan.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JohanAgroFood.Controllers
{
    public class SectorController : Controller
    {
         private ISectorRepository sectorRepository;
           //////////////   sector and common element same
        //constructor
         public SectorController()
        {
            if (sectorRepository == null)
            {
                this.sectorRepository = new DSectorRepository(new JohanAgroFoodDBEntities());
            }
        }
        // GET: Sector
        public ActionResult SectorIndex()
        {
            return View();
        }
        public ActionResult zone()
        {
            return View();
        }
        public ActionResult district()
        {
            return View();
        }

        [HttpPost]
        public JsonResult DeleteSector([System.Web.Http.FromBody] tblCommonElement sector)
        {
            return Json(sectorRepository.DeleteSector(sector), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditSector([System.Web.Http.FromBody] tblCommonElement sector)
        {
            return Json(sectorRepository.EditSector(sector), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveSector(tblCommonElement sector)
        {
            return Json(sectorRepository.SaveSector(sector), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSector(int elemCode)
        {
            var SectorList = sectorRepository.GetSector(elemCode);
            return Json(SectorList, JsonRequestBehavior.AllowGet);
        }
    }
}