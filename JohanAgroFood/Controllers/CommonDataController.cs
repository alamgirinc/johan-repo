﻿using Johan.DATA;
using Johan.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JohanAgroFood.Controllers
{
    public class CommonDataController : Controller
    {
        public JohanAgroFoodDBEntities context = new JohanAgroFoodDBEntities();
        public CommonDataController()
        {
            context.Configuration.ProxyCreationEnabled = false;
        }
        public JsonResult GetAllProduct()
        {               
            var prods = CommonData.GetAllProduct(context);
            return Json(prods, JsonRequestBehavior.AllowGet);
        }
        
    }
}