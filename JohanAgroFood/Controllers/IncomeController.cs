﻿using Johan.DATA;
using Johan.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JohanAgroFood.Controllers
{
    public class IncomeController : Controller
    {
         private IIncomeRepository incomeRepository;

        //constructor
         public IncomeController()
        {
            if (incomeRepository == null)
            {
                this.incomeRepository = new DIncomeRepository(new JohanAgroFoodDBEntities());
            }
        }
        // GET: Income
        public ActionResult IncomeIndex()
        {
            return View();
        }

        [HttpPost]
        public JsonResult DeleteIncome([System.Web.Http.FromBody] tblIncomeSource income)
        {
            return Json(incomeRepository.DeleteIncome(income), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditIncome([System.Web.Http.FromBody] tblIncomeSource income)
        {
            return Json(incomeRepository.EditIncome(income), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveIncome(tblIncomeSource income)
        {
            return Json(incomeRepository.SaveIncome(income), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetIncome()
        {
            var IncomeList = incomeRepository.GetIncome();
            return Json(IncomeList, JsonRequestBehavior.AllowGet);
        }
    }
}