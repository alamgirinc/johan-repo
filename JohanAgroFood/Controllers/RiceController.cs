﻿using Johan.DATA;
using Johan.Repository;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JohanAgroFood.Controllers
{
    public class RiceController : Controller
    {
        private IRiceRepository riceRepository;

        //constructor
        public RiceController()
        {
            if (riceRepository == null)
            {
                this.riceRepository = new DRiceRepository(new JohanAgroFoodDBEntities());
            }
        }
        // GET: Loaner
        public ActionResult RiceInfoIndex()
        {
            return View();
        }
        [HttpPost]
        public JsonResult SaveRiceInfo(tblSell riceInfo)
        {
            return Json(riceRepository.SaveRice(riceInfo), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditRiceInfo(tblSell riceInfo)
        {
            return Json(riceRepository.EditRiceSell(riceInfo), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteRiceInfo(int pk)
        {
            return Json(riceRepository.DeleteRiceSell(pk), JsonRequestBehavior.AllowGet);
        }
           
        public JsonResult GetRiceInfo()
        {
            var sellList = riceRepository.GetRiceInfo();
            return Json(sellList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStock()
        {
            var stkList = riceRepository.GetStock();
            return Json(stkList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadRice(int StockId)
        {
            List<tblProduct> rices = riceRepository.LoadRice(StockId);
            return Json(rices,JsonRequestBehavior.AllowGet);
        }
        public ActionResult rptRiceInfo()
        {
            return View();
        }

        //[HttpPost]
        public ActionResult Preview(string partyId, string from, string to)
        {
            try
            {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reports"), "riceInfo.rdlc");
            if (!System.IO.File.Exists(path)||string.IsNullOrEmpty(partyId)||string.IsNullOrEmpty(from)||string.IsNullOrEmpty(to))
            {
                return View("rptRiceInfo");
            }
            else
            {
                lr.ReportPath = path;
            }
            tblSell riceRpt = new tblSell();
            riceRpt.partyId = Convert.ToInt32(partyId);
            riceRpt.fromDate = Convert.ToDateTime(from);
            riceRpt.toDate = Convert.ToDateTime(to);
            List<object> riceInfLst = riceRepository.GetRiceInfoRpt(riceRpt);
            //ReportDataSource rd = new ReportDataSource("sp_GetRiceInfo", riceInfLst);
            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "datset_GetProductInfo";

            reportDataSource.Value = riceInfLst;
            lr.DataSources.Add(reportDataSource);
            string reportType = "PDF";
            //string reportType1 = "Image";
           // string mimeType;
            string LastmimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + reportType + "</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.5in</MarginTop>" +
            "  <MarginLeft>0.25in</MarginLeft>" +
            "  <MarginRight>0.25in</MarginRight>" +
            "  <MarginBottom>0.5in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;
            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out LastmimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, LastmimeType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public FileContentResult GenerateAndDisplayReport(string bb)
        {
            LocalReport localReport = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reports"), "riceInfo.rdlc");
            localReport.ReportPath = Server.MapPath("~/Reports/student.rdlc");
            List<WorldModel> customerList = new List<WorldModel>();
           
            customerList.Add(new WorldModel(){name="Europe", roll= "Sweden", country= "2001", year= "1823"});
            customerList.Add(new WorldModel() { name = "Europe", roll = "Sweden", year = "2002", country = "1234" });
            customerList.Add(new WorldModel() { country = "Europe", year = "Sweden", roll = "2003", name = "9087" });

            //customerList.Add(new WorldModel("Europe", "Denmark", "2001", "6793"));
            //customerList.Add(new WorldModel("Europe", "Denmark", "2002", "4563"));
            //customerList.Add(new WorldModel("Europe", "Denmark", "2003", "1897"));

            //customerList.Add(new WorldModel("Europe", "Norway", "2001", "5632"));
            //customerList.Add(new WorldModel("Europe", "Norway", "2002", "9870"));
            //customerList.Add(new WorldModel("Europe", "Norway", "2003", "2367"));

            //customerList.Add(new WorldModel("Asia", "India", "2001", "1980"));
            //customerList.Add(new WorldModel("Asia", "India", "2002", "9765"));
            //customerList.Add(new WorldModel("Asia", "India", "2003", "6789"));

            //customerList.Add(new WorldModel("Asia", "Japan", "2001", "9871"));
            //customerList.Add(new WorldModel("Asia", "Japan", "2002", "2987"));
            //customerList.Add(new WorldModel("Asia", "Japan", "2003", "1256"));

            //customerList.Add(new WorldModel("North America", "United States", "2001", "9871"));
            //customerList.Add(new WorldModel("North America", "United States", "2002", "9871"));
            //customerList.Add(new WorldModel("North America", "United States", "2003", "9871"));


            //customerList.Add(new WorldModel("North America", "Canada", "2001", "9871"));
            //customerList.Add(new WorldModel("North America", "Canada", "2002", "9871"));
            //customerList.Add(new WorldModel("North America", "Canada", "2003", "9871"));

            //customerList.Add(new WorldModel("North America", "Mexico", "2001", "9871"));
            //customerList.Add(new WorldModel("North America", "Mexico", "2002", "9871"));
            //customerList.Add(new WorldModel("North America", "Mexico", "2003", "9871"));

            //customerList.Add(new WorldModel("South America", "Brazil", "2001", "9871"));
            //customerList.Add(new WorldModel("South America", "Brazil", "2002", "9871"));
            //customerList.Add(new WorldModel("South America", "Brazil", "2003", "9871"));

            //customerList.Add(new WorldModel("South America", "Columbia", "2001", "9871"));
            //customerList.Add(new WorldModel("South America", "Columbia", "2002", "9871"));
            //customerList.Add(new WorldModel("South America", "Columbia", "2003", "9871"));

            //customerList.Add(new WorldModel("South America", "Argentina", "2001", "9871"));
            //customerList.Add(new WorldModel("South America", "Argentina", "2002", "9871"));
            //customerList.Add(new WorldModel("South America", "Argentina", "2003", "9871"));
            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "DataSet1";
            reportDataSource.Value = customerList;

            localReport.DataSources.Add(reportDataSource);
            string reportType = "Image";
            string mimeType;
            string encoding;
            string fileNameExtension;
            //The DeviceInfo settings should be changed based on the reportType            
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx            
            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>jpeg</OutputFormat>" +
                "  <PageWidth>8.5in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.5in</MarginTop>" +
                "  <MarginLeft>1in</MarginLeft>" +
                "  <MarginRight>1in</MarginRight>" +
                "  <MarginBottom>0.5in</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;
            //Render the report            
            renderedBytes = localReport.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
            //Response.AddHeader("content-disposition", "attachment; filename=NorthWindCustomers." + fileNameExtension); 
                return File(renderedBytes, "image/jpeg");
        }    
    }
}