﻿using Johan.DATA;
using Johan.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JohanAgroFood.Controllers
{
    public class HuskSellController : Controller
    {
        private IHuskSellRepository sellRepository;

        //constructor
        public HuskSellController()
        {
            if (sellRepository == null)
            {
                this.sellRepository = new DHuskSellRepository(new JohanAgroFoodDBEntities());
            }
        }
        // GET: Loaner
        public ActionResult HuskInfoIndex()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SaveHuskInfo(tblSell riceInfo)
        {
            return Json(sellRepository.SaveHusk(riceInfo), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetHuskInfo()
        {
            var sellList = sellRepository.GetHuskInfo();
            return Json(sellList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStock()
        {
            var stkList = sellRepository.GetStock();
            return Json(stkList, JsonRequestBehavior.AllowGet);
        }
    }
}