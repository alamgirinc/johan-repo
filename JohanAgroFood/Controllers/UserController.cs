﻿using Johan.DATA;
using Johan.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JohanAgroFood.Controllers
{
    public class UserController : Controller
    {
        private IUserRepository userRepository;

        //constructor
        public UserController()
        {
            if (userRepository == null)
            {
                this.userRepository = new DUserRepository(new JohanAgroFoodDBEntities());
            }
        }
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetUser()
        {
            var userList = userRepository.GetUser();
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveUser(tblUser user)
        {
            return Json(userRepository.SaveUser(user),JsonRequestBehavior.AllowGet);
        }
        public JsonResult Edit(tblUser objUser)
        {
            return Json(userRepository.Edit(objUser), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int pk)
        {
            return Json(userRepository.Delete(pk), JsonRequestBehavior.AllowGet);
        }
    }
}