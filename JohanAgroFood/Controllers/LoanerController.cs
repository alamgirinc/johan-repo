﻿using Johan.DATA;
using Johan.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JohanAgroFood.Controllers
{
    public class LoanerController : Controller
    {
         private ILoanerRepository loanerRepository;

        //constructor
         public LoanerController()
        {
            if (loanerRepository == null)
            {
                this.loanerRepository = new DLoanerRepository(new JohanAgroFoodDBEntities());
            }
        }
        // GET: Loaner
        public ActionResult LoanerIndex()
        {
            return View();
        }
        
        [HttpPost]
        public JsonResult SaveLoaner(tblLoanar loaner)
        {
            return Json(loanerRepository.SaveLoaner(loaner), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLoaner()
        {
            var loanerList = loanerRepository.GetLoaner();
            return Json(loanerList, JsonRequestBehavior.AllowGet);
        }
    }
}