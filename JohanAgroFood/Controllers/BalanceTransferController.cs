﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Johan.DATA;
using Johan.Repository;

namespace JohanAgroFood.Controllers
{
    public class BalanceTransferController : Controller
    {
      
        private IBalanceRepository balanceRepository;
        //constructor

        public BalanceTransferController()
        {
            if (balanceRepository == null)
            {
                this.balanceRepository = new DBalanceRepository(new JohanAgroFoodDBEntities());
            }
        }

        // GET: /LoanBalance/
        public ActionResult BalanceIndex()
        {
            return View();
        }

        public JsonResult Save(tblPayable objPayable)
        {
            return Json(balanceRepository.SaveBalance(objPayable), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBalanceInfo()
        {
            return Json(balanceRepository.GetBalanceInfo(),JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Edit(tblPayable objPayable)
        {
            return Json(balanceRepository.Edit(objPayable), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(int pk)
        {
            return Json(balanceRepository.Delete(pk), JsonRequestBehavior.AllowGet);
        }
	}
}