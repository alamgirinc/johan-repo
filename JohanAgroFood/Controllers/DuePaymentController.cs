﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Johan.DATA;
using Johan.Repository;
using Johan.Repository.DAL;
using Johan.Repository.IDAL;

namespace JohanAgroFood.Controllers
{
    public class DuePaymentController : Controller
    {
        private IDuePaymentRepository duePaymentRepository;
        //constructor

        public DuePaymentController()
        {
            if (duePaymentRepository == null)
            {
                this.duePaymentRepository = new DDuePaymentRepository(new JohanAgroFoodDBEntities());
            }
        }
        // GET: /DuePayment/
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult Save(tblIncomeSource objDueIncomeSource)
        {
            return Json(duePaymentRepository.Save(objDueIncomeSource), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDuePayment(int objPartyId)
        {
            return Json(duePaymentRepository.GetDuePayment(objPartyId), JsonRequestBehavior.AllowGet);
        }
	}
}