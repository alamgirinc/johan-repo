﻿using Johan.DATA;
using Johan.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JohanAgroFood.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository ProductRepository;

        //constructor
        public ProductController()
        {
            if (ProductRepository == null)
            {
                this.ProductRepository = new DProductRepository(new JohanAgroFoodDBEntities());
            }
        }
        // GET: Login
        public ActionResult ProductIndex()
        {
            return View();
        }

        public JsonResult GetProduct(tblProduct prod)
        {
            var productList = ProductRepository.GetProduct(prod);
            return Json(productList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveProduct(tblProduct product)
        {
            return Json(ProductRepository.SaveProduct(product), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditProduct([System.Web.Http.FromBody] tblProduct product)
        {
            return Json(ProductRepository.EditProduct(product), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteProduct(int pk)
        {
            return Json(ProductRepository.DeleteProduct(pk), JsonRequestBehavior.AllowGet);
        }
    }
}