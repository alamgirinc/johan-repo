﻿using Johan.DATA;
using Johan.Repository;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JohanAgroFood.Controllers
{
    public class ParticleSellController : Controller
    {
        private IParticleSellRepository sellRepository;

        //constructor
        public ParticleSellController()
        {
            if (sellRepository == null)
            {
                this.sellRepository = new DParticleSellRepository(new JohanAgroFoodDBEntities());
            }
        }
        // GET: Loaner
        public ActionResult particleInfoIndex()
        {
            return View();
        }

        public ActionResult rptParticleInfo()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SaveParticleInfo(tblSell riceInfo)
        {
            return Json(sellRepository.SaveParticle(riceInfo), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetParticleInfo()
        {
            var sellList = sellRepository.GetParticleInfo();
            return Json(sellList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStock()
        {
            var stkList = sellRepository.GetStock();
            return Json(stkList, JsonRequestBehavior.AllowGet);
        }
        // stock portion

        public ActionResult ParticleStock()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SaveParticleStock(STK_Balance particleStk)
        {
            return Json(sellRepository.SaveParticleStock(particleStk), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditParticleInfo(tblSell objParticleInfo)
        {
            return Json(sellRepository.EditParticleSell(objParticleInfo), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteParticleInfo(int pk)
        {
            return Json(sellRepository.DeleteParticleSell(pk), JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        public ActionResult Preview(string partyId, string from, string to)
        {
            try
            {
                LocalReport lr = new LocalReport();
                string path = Path.Combine(Server.MapPath("~/Reports"), "particleInfo.rdlc");
                if (!System.IO.File.Exists(path) || string.IsNullOrEmpty(partyId) || string.IsNullOrEmpty(from) || string.IsNullOrEmpty(to))
                {
                    return View("rptRiceInfo");
                }
                else
                {
                    lr.ReportPath = path;
                }
                tblSell prtlRpt = new tblSell();
                prtlRpt.partyId = Convert.ToInt32(partyId);
                prtlRpt.fromDate = Convert.ToDateTime(from);
                prtlRpt.toDate = Convert.ToDateTime(to);
                List<object> particleInfLst = sellRepository.GetParticleInfoRpt(prtlRpt);
                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "dataset_GetParticleInfo";

                reportDataSource.Value = particleInfLst;
                lr.DataSources.Add(reportDataSource);
                string reportType = "PDF";
                //string reportType1 = "Image";
                string mimeType;
                string LastmimeType;
                string encoding;
                string fileNameExtension;

                string deviceInfo =

                "<DeviceInfo>" +
                "  <OutputFormat>" + reportType + "</OutputFormat>" +
                "  <PageWidth>8.5in</PageWidth>" +
                "  <PageHeight>11in</PageHeight>" +
                "  <MarginTop>0.5in</MarginTop>" +
                "  <MarginLeft>0.25in</MarginLeft>" +
                "  <MarginRight>0.25in</MarginRight>" +
                "  <MarginBottom>0.5in</MarginBottom>" +
                "</DeviceInfo>";

                Warning[] warnings;
                string[] streams;
                byte[] renderedBytes;
                renderedBytes = lr.Render(
                    reportType,
                    deviceInfo,
                    out LastmimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);
                return File(renderedBytes, LastmimeType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        [HttpPost]
        public JsonResult DeleteParticleStock([System.Web.Http.FromBody] STK_Balance particleStk)
        {
            return Json(sellRepository.DeleteParticleStock(particleStk), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditParticleStock([System.Web.Http.FromBody] STK_Balance particleStk)
        {
            return Json(sellRepository.EditParticleStock(particleStk), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetParticleStock()
        {
            var riceStkLst = sellRepository.GetParticleStock();
            return Json(riceStkLst, JsonRequestBehavior.AllowGet);
        }

    }
}