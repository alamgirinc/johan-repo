﻿app.controller("riceStockController", ["$scope", "riceStockService", "convertSvc", "productService",
    function (scope, riceStockService, convertSvc, productService) {
    scope.riceStock = {};
    scope.submitted = false;
    scope.riceStocks = [];
    scope.stocks = [];
    scope.rices = [];
    scope.isEdit = null;

    //methods
    scope.save = save;
    scope.edit = edit;
    scope.delete = deleteRiceStock;
    init();

    //GetUser();
    //GetAll();

    //To Get All Records  
    function init() {
        getRice();
        getStock();
        initialize();
    }

    function initialize() {
        scope.riceStock = {};
        scope.isEdit = false;
        getRiceStocks();

    }

    function getRice() {
        var rice = { parentId: 2 };
        var result = productService.getProduct(rice);
        result.then(function (data) {
            scope.rices = data;
        }, function (e) {
            alert(e);
        });
    }

    function getStock() {
        var result = riceStockService.getStock();
        result.then(function (data) {
            scope.stocks = data;
        }, function (e) {
            alert(e);
        });
    }

    function getRiceStocks() {
        var result = riceStockService.getRiceStock();
        result.then(function (data) {
            scope.riceStocks = data;
            scope.riceStocks.forEach(function (con) {
                con.createDate = convertSvc.toDate(con.createDate);
                con.sckQuantity = convertSvc.ConvtEngToBang(con.sackQuantity);
                //con.sckWeight = convertSvc.ConvtEngToBang(con.sackWeight);
            });
        }, function (e) {
            alert(e);
        });
    }

    function edit(inc) {
        scope.isEdit = true;
        scope.riceStock = inc;
        //scope.riceStock.date = convertSvc.toDate(inc.date);

        scope.sector.ID = inc.srcDescId;
        scope.sector.elementName = inc.sourceName;
    }

    function save() {
        scope.submitted = true;
        if (scope.riceStockForm.$valid) {
            var result = null;
            var operation = null;
            if (scope.isEdit) {
                //scope.riceStock.sourceName = "";
                result = riceStockService.edit(scope.riceStock);
                operation = "save";
            }
            else {
                result = riceStockService.save(scope.riceStock);
                operation = "edit";
            }
            result.then(function (data) {
                alert("ডাটা সেভ হয়েছে");
                convertSvc.updateCollection(scope.riceStocks, scope.riceStock, operation, "ID");
                initialize();
            }, function (e) {
                alert(e);
            });
        }
    }

    function deleteRiceStock(sec) {
        var result = riceStockService.deleteRiceStock(sec);
        result.then(function (data) {
            alert("ডাটা ডিলিট হয়েছে");
            initialize();
        }, function () {
            alert('ডিলিট হয়নি। ');
        });
    }

}]);