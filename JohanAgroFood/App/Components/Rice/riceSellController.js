﻿app.controller("riceSellController", ["$scope", "riceSellService", "partyService","convertSvc",
    function (scope, riceSellService, partyService, convertSvc) {
        scope.riceInfo = {};
        scope.riceReport = {};
        scope.isCash = null;
        scope.isEdit = null;
        scope.rices = [];
        scope.stocks = [];
        scope.riceInfos = [];
        scope.parties = [];
    //var template = '<div><input type="text" ng-change="grid.appScope.selectSell(row.entity)" /></div>';
    
        var templateView = '<a href="#" ng-click="grid.appScope.edit(row.entity)"><span class="glyphicon glyphicon-pencil">পরিবর্তন</span></a> ' +
        '<a href="#" ng-click="grid.appScope.deleteSell(row.entity)"><span class="glyphicon glyphicon-trash">মূছুন</span></a>';
    scope.gridOptions = {
        data: 'riceInfos',
        columnDefs: [
          //{ field: 'ID', displayName: 'ID' },
          { field: 'date', displayName: 'তারিখ' },
          { field: 'noBag', displayName: 'ব্যাগের সংখ্যা' },
          { field: 'partyName', displayName: 'পার্টির নাম' },
          { field: 'productName', displayName: 'চালের নাম' },
          { field: 'stockName', displayName: 'স্টক নাম' },
          { field: 'qty', displayName: 'চালের পরিমাণ' },
          { field: 'unit', displayName: 'একক' },
          { field: 'uPrice', displayName: 'একক মূল্য' },
          { field: 'isSelect', displayName: '', cellTemplate: templateView }
        ],
        enableRowSelection: true,
        enableSelectAll: true,
        multiSelect: true
    };


    scope.save = save;
    scope.edit = edit;
    scope.deleteSell = deleteSell;
    scope.getTotal = getTotal;
    scope.showReport = showReport;
    scope.loadRice = loadRice;
    scope.cashParty = cashParty;

    init();

    //GetUser();
    //GetAll();


    //To Get All Records  
    function init() {
        getParty();
        getStock();
        getRiceSellInfo();
        initialize();
    }

    function initialize()
    {
        scope.isEdit = false;
        scope.riceInfo = {};
        scope.riceInfo.isMon = false;
        scope.isCash = false;
    }

    function edit(row) {
        scope.isEdit = true;
        scope.riceInfo = row;
    }

    function getTotal()
    {
        if (scope.riceInfo.isMon) {
            scope.riceInfo.unit = 2;
            var mon = scope.riceInfo.quantity / 40;
            scope.riceInfo.totPrice = mon * scope.riceInfo.unitPrice * scope.riceInfo.noOfBag;
        }
        else {
            scope.riceInfo.unit = 1;
            scope.riceInfo.totPrice = scope.riceInfo.quantity * scope.riceInfo.unitPrice * scope.riceInfo.noOfBag;
        }
        scope.riceInfo.totalpr = convertSvc.ConvtEngToBang(scope.riceInfo.totPrice);
    }
    
    function cashParty()
    {
        if (scope.isCash) {
            scope.riceInfo.partyId = 3010;
            scope.riceInfo.partyName= "নগদ";
        }
    }

    function deleteSell(row) {
        var result = riceSellService.deleteSell(row);
        result.then(function (data) {
            alert("ডাটা ডিলিট হয়েছে");
            convertSvc.updateCollection(scope.riceInfos, row, "delete", "ID");
        }, function (e) {
            alert(e);
        });
    }

    function getParty() {
        var result = partyService.getParty();
        result.then(function (data) {
            scope.parties = data;
        }, function (e) {
            alert(e);
        });
    }

    function loadRice()
    {
        if (scope.riceInfo.stockId == null) { scope.rices = []; return; }
        var result = riceSellService.loadRice(scope.riceInfo.stockId);
        result.then(function (data) {
            scope.rices = data;
        }, function (e) {
            alert(e);
        });
    }

    function getStock()
    {
        var result = riceSellService.getStock();
        result.then(function (data) {
            scope.stocks = data;
        }, function (e) {
            alert(e);
        });
    }

    function getRiceSellInfo() {
        var result = riceSellService.getRiceSellInfo();
        result.then(function (data) {
            scope.riceInfos = data;
            scope.riceInfos.forEach(function (r) {
                if (r.unit==2) {
                    r.isMon = true;
                }
                else {
                    r.isMon = false;
                }
                r.date = convertSvc.toDate(r.date);
                r.qty = convertSvc.ConvtEngToBang(r.quantity);
                r.uPrice = convertSvc.ConvtEngToBang(r.unitPrice);
                r.noBag = convertSvc.ConvtEngToBang(r.noOfBag);
                r.paidAmnt = convertSvc.ConvtEngToBang(r.paidAmount);
                r.transCost = convertSvc.ConvtEngToBang(r.transportCost);
            });
        }, function (e) {
            alert(e);
        });
    }
    
    function save() {
        if (!scope.riceInfoForm.$valid) {
            return;
        }
        var result = null;
        var operation = null;
        if (scope.isEdit) {
            operation = "edit";
            result = riceSellService.edit(scope.riceInfo);
        }
        else {
            operation = "save";
            result = riceSellService.save(scope.riceInfo);
        }
        result.then(function (data) {
            alert("ডাটা সেভ হয়েছে");
            scope.riceInfo.ID = data;
            convertSvc.updateCollection(scope.riceInfos, scope.riceInfo, operation, "ID");
            initialize();
        }, function (e) {
            alert(e);
        });
    }

    function showReport()
    {
        var result = riceSellService.showReport(scope.riceReport);
    }
    
}]);