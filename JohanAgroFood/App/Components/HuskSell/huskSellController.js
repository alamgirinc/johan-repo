﻿app.controller("huskSellController", ["$scope", "huskSellService", "partyService","convertSvc",
    function (scope, huskSellService, partyService, convertSvc) {//productService,
        scope.huskInfo = {};
        //scope.riceParty = {};
        //scope.riceStock = {};
        //scope.riceProduct = {};
        scope.husks = [];
    scope.stocks = [];
    scope.huskInfos = [];
    scope.parties = [];
    var template = '<div><input type="text" ng-change="grid.appScope.selectSell(row.entity)" /></div>';
    scope.gridOptions = {

        data: 'huskInfos',
        columnDefs: [
          { field: 'ID', displayName: 'ID' },
          { field: 'date', displayName: 'Date' },
          { field: 'noBag', displayName: 'No Of Bag' },
          { field: 'partyName', displayName: 'Party Name' },
          { field: 'productName', displayName: 'Product Name' },
          { field: 'stockName', displayName: 'Stock Name' },
          { field: 'qty', displayName: 'Quantity' },
          { field: 'unit', displayName: 'unit' },
          { field: 'prc', displayName: 'unitPrice' }
],
        enableRowSelection: true,
        enableSelectAll: true,
        multiSelect: true
    };


    scope.selectSell = selectSell;
    scope.save = save;
    init();

    //GetUser();
    //GetAll();


    //To Get All Records  
    function init() {
        getParty();
        //getHusk();
        getStock();
        getHuskSellInfo();
    }
    function initialize() {
        scope.isEdit = false;
        scope.huskInfo = {};
    }

    function selectSell(row) {
        scope.huskInfo = row;
    }

    function getParty() {
        var result = partyService.getParty();
        result.then(function (data) {
            scope.parties = data;
        }, function (e) {
            alert(e);
        });
    }


    function getStock()
    {
        var result = huskSellService.getStock();
        result.then(function (data) {
            scope.stocks = data;
        }, function (e) {
            alert(e);
        });
    }

    function getHuskSellInfo() {
        var result = huskSellService.getHuskSellInfo();
        result.then(function (data) {
            scope.huskInfos = data;
            scope.huskInfos.forEach(function (hsk) {
                hsk.date = convertSvc.toDate(hsk.date); 
                hsk.qty = convertSvc.ConvtEngToBang(hsk.quantity);
                hsk.prc = convertSvc.ConvtEngToBang(hsk.unitPrice);
                hsk.noBag = convertSvc.ConvtEngToBang(hsk.noOfBag);

            });

        }, function (e) {
            alert(e);
        });
    }
    
    function save(huskInfo) {
        if (!scope.huskSellForm.$valid) {
            return;
        }
        var result = null;
        var operation = null;
        huskInfo.productId = 4;// 4 is id of husk

        if (scope.isEdit) {
            operation = "edit";
            result = huskSellService.edit(huskInfo);
        }
        else {
            operation = "save";
            result = huskSellService.save(huskInfo);
        }
        result.then(function (data) {
            alert("ডাটা সেভ হয়েছে");
            scope.riceInfo.ID = data;
            convertSvc.updateCollection(scope.huskInfos, scope.huskInfo, operation, "ID");
            initialize();
        }, function (e) {
            alert(e);
        });

        var result = null;
        var operation = null;
        if (scope.isEdit) {
            operation = "edit";
            result = riceSellService.edit(scope.riceInfo);
        }
        else {
            operation = "save";
            result = riceSellService.save(scope.riceInfo);
        }
        result.then(function (data) {
            alert("ডাটা সেভ হয়েছে");
            scope.riceInfo.ID = data;
            convertSvc.updateCollection(scope.riceInfos, scope.riceInfo, operation, "ID");
            initialize();
        }, function (e) {
            alert(e);
        });
    }
    
}]);