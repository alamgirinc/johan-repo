﻿app.service("huskSellService", ["$http", "baseDataSvc", function ($http, baseDataSvc) {
    //service instant
    var dataSvc = {
        getHuskSellInfo: getHuskSellInfo,
        getStock:getStock,
        save: save
    };
    return dataSvc;

    function getHuskSellInfo() {
        try {
            return baseDataSvc.executeQuery('/HuskSell/GetHuskInfo', {});
        } catch (e) {
            throw e;
        }
    }

    function getStock() {
        try {
            return baseDataSvc.executeQuery('/RiceSell/GetStock', {});
        } catch (e) {
            throw e;
        }
    }

    function save(objHuskInfo) {
        try {
            return baseDataSvc.executeQuery('/HuskSell/SaveHuskInfo', objHuskInfo);
        } catch (e) {
            throw e;
        }
    }
}]);