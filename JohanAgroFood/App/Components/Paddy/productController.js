﻿app.controller("productController", ["$scope", "productService", function (scope, productService) {
    scope.product = {};
    scope.products = [];
    init();

    //To Get All Records  
    function init() {
        getProduct();
    }

    function getPartys() {
        var result = partyService.getPartys();
        result.then(function (data) {
            scope.parties = data;
        }, function (e) {
            alert(e);
        });
    }
    function getZones() {
        var result = zoneService.getZones();
        result.then(function (data) {
            scope.zones = data;
        }, function (e) {
            alert(e);
        });
    }
    function getProduct() {
        var result = productService.getProduct(null);
        result.then(function (data) {
            scope.products = data;
        }, function (e) {
            alert(e);
        });
    }
    scope.edit = edit;

    function edit(location) {
        $scope.LOCATIONID = location.LOCATIONID;
        $scope.LOCATIONCODE = location.LOCATIONCODE;
        $scope.LOCATIONNAME = location.LOCATIONNAME;
        $scope.DISTRICTID = location.DISTRICTID;
        $scope.Operation = "Update";
    }
    scope.add = add;
    function add() {
        $scope.LOCATIONID = "";
        $scope.LOCATIONCODE = "";
        $scope.LOCATIONNAME = "";
        $scope.DISTRICTID = $scope.Districts[-1];

    }

    scope.Save = save;
    function save(party) {
        var result = partyService.save(party);
        result.then(function (data) {
            alert("ডাটা সেভ হয়েছে");
            getUsers();
        }, function (e) {
            alert(e);
        });
    }
    scope.delete = deleteUser;

    function deleteUser(location) {
        var getMSG = userService.Delete(location.LOCATIONID);
        getMSG.then(function (messagefromController) {
            GetAll();
            alert(messagefromController.data);
            //CE-150328-ANIS
            $window.location.reload();
        }, function () {
            alert('ডিলিট হয়নি। ');
        });
    }


    //To Get All District  
    function GetAllDistrict() {
        var Data = userService.getDistrict();
        Data.then(function (district) {
            $scope.Districts = district.data;
        }, function () {
            alert('Error District');
        });
    }


}]);