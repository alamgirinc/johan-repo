﻿app.service("productService", ["$http", "baseDataSvc", function ($http, baseDataSvc) {
    //service instant
    var dataSvc = {
        getProduct: getProduct
    };
    return dataSvc;

    function getProduct(product) {
        try {
            return baseDataSvc.executeQuery('/Product/GetProduct', product);
        } catch (e) {
            throw e;
        }
    }

    function save(objProduct) {
        try {
            return baseDataSvc.executeQuery('/Product/SaveProduct', objProduct);
        } catch (e) {
            throw e;
        }
    }
}]);