﻿app.controller("duePaymentCltr", ["$scope", "partyService","convertSvc","sectorService","duePaymentSvc",
function (scope, partyService, convertSvc, sectorService, duePaymentSvc) {
    scope.duePayment = {};
    scope.duePayments = [];
        scope.parties = [];
        scope.sectors = [];
    scope.sector = {};
    scope.save = save;
   
    scope.getDuePaymentInfos = getDuePaymentInfos;
        //scope.searchDuePayment = searchDuePayment;
        scope.selectSector = selectSector;
        init();


        function init() {
            //getDuePaymentInfos();
            initialize();
            getParties();
            getSectors();
        }

        function initialize() {
            scope.duePayment = {};
            scope.sector = {};
        }
         
        function getDuePaymentInfos() {
            
            var result = duePaymentSvc.getDuePayment(scope.duePayment.partyId);
            result.then(function (data) {
                scope.duePayments = data;
                scope.duePayments.forEach(function (con) {
                    con.date = convertSvc.toDate(con.date);
                    con.amt = convertSvc.ConvtEngToBang(con.amount);

                });
            }, function (e) {
                alert(e);
            });
        }
        function selectSector() {
            scope.duePayment.srcDescId = scope.sector.ID;
            scope.duePayment.sourceName = scope.sector.elementCode;

        }
        function getParties() {
            var result = partyService.getParty();
            result.then(function (data) {
                scope.parties = data;
                scope.parties.forEach(function (con) {
                    con.contact = convertSvc.ConvtEngToBang(con.contactNo);
                    con.cash = con.isCashParty ? "হ্যা" : "না";
                });
            }, function (e) {
                alert(e);
            });
        }
        function getSectors() {
            var result = sectorService.getSector();
            result.then(function (data) {
                scope.sectors = data;
            }, function (e) {
                alert(e);
            });
        }
        function save() {
            if (!scope.duePaymentForm.$valid) {
                return;
            }
            var result = duePaymentSvc.save(scope.duePayment);
            result.then(function () {
                alert("ডাটা সেভ হয়েছে");
                getDuePaymentInfos();
                initialize();
            }, function (e) {
                alert(e);
            });
        }


    }]);