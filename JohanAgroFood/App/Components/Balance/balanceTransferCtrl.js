﻿app.controller("balanceTransferCtrl", ["$scope", "balanceTransferSvc","convertSvc",
    function (scope, balanceTransferSvc,convertSvc) {
        scope.balanceInfo = {};
        scope.balanceInfos = [];
        scope.parties = [];
        scope.edit = edit;

        //methods
        //scope.edit = edit;
        scope.save = save;
        scope.delete = deleteUser;
        init();

    //To Get All Records
    function init() {
        initialize();
        getBalanceInfos();
        getParties();
    }

    function initialize() {
        scope.balanceInfo = {};
        scope.isEdit = false;
    }

    function save() {
        if (!scope.balanceInfoForm.$valid) {
            return;
        }
        var result = null;
        var operation = null;
        if (scope.isEdit) {
            operation = "edit";
            result = balanceTransferSvc.edit(scope.balanceInfo);
        }
        else {
            operation = "save";
            result = balanceTransferSvc.save(scope.balanceInfo);
        }
        result.then(function () {
            alert("ডাটা সেভ হয়েছে");
            getBalanceInfos();
            initialize();
        }, function (e) {
            alert(e);
        });
    }
    function getBalanceInfos() {
        var result = balanceTransferSvc.getBalanceInfo();
        result.then(function(data) {
            scope.balanceInfos = data;
            scope.balanceInfos.forEach(function(r) {
                r.date = convertSvc.toDate(r.date);
            });
        },function(e) {
                alert(e);
        });
    }
  
    function getParties() {
        var result = balanceTransferSvc.getParty();
        result.then(function(data) {
            scope.parties = data;
        },function(e) {
            alert(e);
        });
    }
    function edit(row) {
        scope.isEdit = true;
        scope.balanceInfo = row;
    }
    function deleteUser(row) {
        var result = balanceTransferSvc.deleteUser(row);
        result.then(function (data) {
            alert("ডাটা ডিলিট হয়েছে");
            convertSvc.updateCollection(scope.balanceInfos, row, "delete", "ID");
        }, function (e) {
            alert(e);
        });
    }






    }]);
