﻿app.controller("productController", ["$scope", "productService", function (scope, productService) {
    scope.product = null;
    scope.products = [];
    scope.parents = [];

    //methods
    scope.edit = edit;
    scope.save = save;
    scope.delete = deleteProduct;


    init();

    //To Get All Records  
    function init() {
        getProduct();
        getParents();
        initialize();
    }
    function initialize()
    {
        scope.product = {};
    }

    function getParents() {
        var result = productService.getParents();
        result.then(function (data) {
            scope.parents = data;
        }, function (e) {
            alert(e);
        });
    }
    function getProduct() {
        var result = productService.getProduct(null);
        result.then(function (data) {
            scope.products = data;
        }, function (e) {
            alert(e);
        });
    }

    function edit(prod) {
        scope.isEdit = true;
        scope.product = prod;
    }

    function save() {
        if (!scope.productForm.$valid) {
            return;
        }
        var result=null;
        var operation = "save";
        if (scope.isEdit) {
            operation = "edit";
            result = productService.edit(scope.product);
        }
        else {
            result = productService.save(scope.product);
        }
        result.then(function (data) {
            alert("ডাটা সেভ হয়েছে");
            getProduct();
            initialize();
        }, function (e) {
            alert(e);
        });
    }

    function deleteProduct(prod) {
        var result = productService.deleteProduct(prod);
        result.then(function (data) {
            alert("ডাটা ডিলিট হয়েছে");
            getProduct();
            initilize();
        }, function (e) {
            alert(e);
        });
    }
    
}]);