﻿app.service("prodStockSvc", ["$http", "baseDataSvc", function ($http, baseDataSvc) {
    //service instant
    var dataSvc = {
        getProdStocks: getProdStocks,
        getStock:getStock,
        save: save,
        edit: edit,
        deleteProdStock: deleteProdStock,
        getAllProduct: getAllProduct
    };
    return dataSvc;

    function getAllProduct()
    {
        try {
            return baseDataSvc.executeQuery('/CommonData/GetAllProduct', {});
        } catch (e) {
            throw e;
        }
    }

    function deleteProdStock(objProdStock) {
        try {
            return baseDataSvc.executeQuery('/Stock/DeleteProdStock', { prodStk: objProdStock });
        } catch (e) {
            throw e;
        }
    }

    function edit(objProdStock)
    {
        try {
            return baseDataSvc.executeQuery('/Stock/EditProdStock', { prodStk: objProdStock });
        } catch (e) {
            throw e;
        }
    }

    function getStock() {
        try {
            return baseDataSvc.executeQuery('/Rice/GetStock', {});
        } catch (e) {
            throw e;
        }
    }

    function getProdStocks() {
        try {
            return baseDataSvc.executeQuery('/Stock/GetProdStocks', {});
        } catch (e) {
            throw e;
        }
    }

    function save(objProdStock) {
        try {
            return baseDataSvc.executeQuery('/Stock/SaveProdStock', objProdStock);
        } catch (e) {
            throw e;
        }
    }
}]);