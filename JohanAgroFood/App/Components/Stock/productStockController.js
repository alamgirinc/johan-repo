﻿app.controller("prodStockCtrl", ["$scope", "prodStockSvc", "convertSvc", "productService","$filter",
    function (scope, prodStockSvc, convertSvc, productService, $filter) {
        scope.prodStock = {};
    scope.submitted = false;
    scope.parents = [];
    scope.stocks = [];
    scope.prducts = [];
    scope.allProducts = [];
    scope.isEdit = null;
    scope.prodStocks = [];

    //methods
    scope.save = save;
    scope.edit = edit;
    scope.deleteProdStock = deleteProdStock;
    scope.getChildProd = getChildProd;
    init();

    //GetUser();
    //GetAll();

    //To Get All Records  
    function init() {
        //getParents();
        getStock();
        getAllProduct();
        initialize();
    }

    function initialize() {
        scope.prodStock = {};
        scope.isEdit = false;
        getProdStocks();

    }
    function getChildProd() {
        scope.prducts = $filter('filter')(scope.allProducts, { parentId: scope.prodStock.parentId });
        //var result = productService.getProduct(scope.prodStock);
        //result.then(function (data) {
        //    scope.prducts = data;
        //}, function (e) {
        //    alert(e);
        //});
    }
    function getAllProduct() {
        var result = prodStockSvc.getAllProduct();
        result.then(function (data) {
            scope.parents = $filter('filter')(data, { parentId: 0 });
            scope.allProducts = $filter('filter')(data, { parentId: '!0' });
        }, function (e) {
            alert(e);
        });
    }

    //function getParents() {
    //    var result = productService.getParents();
    //    result.then(function (data) {
    //        scope.parents = data;
    //    }, function (e) {
    //        alert(e);
    //    });
    //}
    function getStock() {
        var result = prodStockSvc.getStock();
        result.then(function (data) {
            scope.stocks = data;
        }, function (e) {
            alert(e);
        });
    }

    function getProdStocks() {
        var result = prodStockSvc.getProdStocks();
        result.then(function (data) {
            scope.prodStocks = data;
            scope.prodStocks.forEach(function (con) {
                con.createDate = convertSvc.toDate(con.createDate);
                con.sckQuantity = convertSvc.ConvtEngToBang(con.sackQuantity);
                con.sckWeight = convertSvc.ConvtEngToBang(con.sackWeight);
            });
        }, function (e) {
            alert(e);
        });
    }

    function edit(inc) {
        scope.isEdit = true;
        scope.prducts = scope.allProducts;
        scope.prodStock = inc;
        scope.prodStock.date = null;

        scope.sector.ID = inc.srcDescId;
        scope.sector.elementName = inc.sourceName;
    }

    function save() {
        scope.submitted = true;
        if (scope.prodStockForm.$valid) {
            var result = null;
            var operation = null;
            if (scope.isEdit) {
                //scope.prodStock.sourceName = "";
                result = prodStockSvc.edit(scope.prodStock);
                operation = "edit";
            }
            else {
                result = prodStockSvc.save(scope.prodStock);
                operation = "save";
            }
            result.then(function (data) {
                alert("ডাটা সেভ হয়েছে");
                convertSvc.updateCollection(scope.parents, scope.prodStock, operation, "ID");
                initialize();
            }, function (e) {
                alert(e);
            });
        }
    }

    function deleteProdStock(sec) {
        var result = prodStockSvc.deleteProdStock(sec);
        result.then(function (data) {
            alert("ডাটা ডিলিট হয়েছে");
            initialize();
        }, function () {
            alert('ডিলিট হয়নি। ');
        });
    }

}]);