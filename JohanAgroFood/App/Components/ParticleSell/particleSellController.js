﻿app.controller("particleSellController", ["$scope", "particleSellService", "productService", "partyService","convertSvc",
function (scope, particleSellService, productService, partyService,convertSvc) {
    scope.particleInfo = {};
    scope.particleReport = {};
        scope.isEdit = false;
        scope.particles = [];
        scope.stocks = [];
        scope.particleInfos = [];
        scope.parties = [];
    //var template = '<div><input type="text" ng-change="grid.appScope.selectSell(row.entity)" /></div>';
    var templateView = '<a href="#" ng-click="grid.appScope.edit(row.entity)"><span class="glyphicon glyphicon-pencil"></span></a> ' +
        '<a href="#" ng-click="grid.appScope.deleteSell(row.entity)">Del</span></a>';//<span class="glyphicon glyphicon-trash">
    scope.gridOptions = {
        data: 'particleInfos',
        columnDefs: [
          { field: 'date', displayName: 'তারিখ' },
          { field: 'noBag', displayName: 'ব্যাগের সংখ্যা' },
          { field: 'partyName', displayName: 'পার্টির নাম' },
          { field: 'productName', displayName: 'চালের নাম' },
          { field: 'stockName', displayName: 'স্টক নাম' },
          { field: 'qty', displayName: 'চালের পরিমাণ(কেজি)' },
          { field: 'uPrice', displayName: 'প্রতি কেজির মূল্য' },
          { field: 'isSelect', displayName: '', cellTemplate: templateView }
],
        enableRowSelection: true,
        enableSelectAll: true,
        multiSelect: true
    };


    scope.save = save;
    scope.getTotal = getTotal;
    scope.edit = edit;
    scope.deleteSell = deleteSell;

    init();

    //GetUser();
    //GetAll();


    //To Get All Records  
    function init() {
        getParty();
        getparticle();
        getStock();
        getParticleSellInfo();
        initialize();

    }
    function initialize() {
        scope.isEdit = false;
        scope.particleInfo = {};
        scope.particleInfo.isMon = false;
    }
    function edit(row) {
        scope.isEdit = true;
        scope.particleInfo = row;
    }
    function deleteSell(row) {
        var result = particleSellService.deleteSell(row);
        result.then(function (data) {
            alert("Deleted successfully");
            convertSvc.updateCollection(scope.particleInfos, row, "delete", "ID");
        }, function (e) {
            alert(e);
        });
    }
    function getParty() {
        var result = partyService.getParty();
        result.then(function (data) {
            scope.parties = data;
        }, function (e) {
            alert("getParty error");
        });
    }

    function getparticle()
    {
        var particle = { parentId: 8 };// 8 for particle
        var result = productService.getProduct(particle);
        result.then(function (data) {
            scope.particles = data;
        }, function (e) {
            alert("getparticle error");
        });
    }

    function getStock()
    {
        var result = particleSellService.getStock();
        result.then(function (data) {
            scope.stocks = data;
        }, function (e) {
            alert("getStock error");
        });
    }

    function getParticleSellInfo() {
        var result = particleSellService.getParticleSellInfo();
        result.then(function (data) {
            scope.particleInfos = data;
            scope.particleInfos.forEach(function (r) {
                if (r.unit==2) {
                    r.isMon = true;
                }
                else {
                    r.isMon = false;
                }
                r.date = convertSvc.toDate(r.date);
                r.noBag = convertSvc.ConvtEngToBang(r.noOfBag);
                r.uPrice = convertSvc.ConvtEngToBang(r.unitPrice);
                r.qty = convertSvc.ConvtEngToBang(r.quantity);
                r.paidAmnt = convertSvc.ConvtEngToBang(r.paidAmount);
                r.transCost = convertSvc.ConvtEngToBang(r.transportCost);
        }, function (e) {
            alert("getParticleSellInfo error");
        });
        });
        }
        
    
    function getTotal() {
        if (scope.particleInfo.isMon) {
            scope.particleInfo.unit = 2;
            var mon = scope.particleInfo.quantity / 40;
            scope.particleInfo.totPrice = mon * scope.particleInfo.unitPrice * scope.particleInfo.noOfBag;
        }
        else {
            scope.particleInfo.unit = 1;
            scope.particleInfo.totPrice = scope.particleInfo.quantity * scope.particleInfo.unitPrice * scope.particleInfo.noOfBag;
        }
        scope.particleInfo.totalpr = convertSvc.ConvtEngToBang(scope.particleInfo.totPrice);
    }
    function save() {
        var result = null;
        var operation = null;
        if (scope.isEdit) {
            operation = "edit";
            result = particleSellService.edit(scope.particleInfo);
        }
        else {
            operation = "save";
            result = particleSellService.save(scope.particleInfo);
        }
        result.then(function (data) {
            alert("ডাটা সেভ হয়েছে");
            scope.particleInfo.ID = data;
            convertSvc.updateCollection(scope.particleInfos, scope.particleInfo, operation, "ID");
            initialize();
        }, function (e) {
            alert(e);
        });
    }
    
}]);