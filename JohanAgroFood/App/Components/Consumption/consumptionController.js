﻿app.controller("consumptionController", ["$scope", "consumptionService", "sectorService", "convertSvc",
    function (scope, consumptionService, sectorService, convertSvc) {
    scope.consumption = {};
    scope.sector = {};
    scope.submitted = false;
    scope.consumptions = [];
    scope.sectors = [];
    scope.isEdit = null;

    //methods
    scope.save = save;
    scope.edit = edit;
    scope.delete = deleteConsumption;
    scope.selectSector = selectSector;
    init();

    //GetUser();
    //GetAll();

    function selectSector() {
        scope.consumption.srcDescId = scope.sector.ID;
        scope.consumption.sourceName = scope.sector.elementName;
    }

    //To Get All Records  
    function init() {
        initialize();
    }

    function initialize() {
        scope.consumption = {};
        scope.isEdit = false;
        getSectors();
        getConsumptions();

    }

    function getSectors() {
        var result = sectorService.getSector();
        result.then(function (data) {
            scope.sectors = data;
        }, function (e) {
            alert(e);
        });
    }

    function getConsumptions() {
        var result = consumptionService.getConsumption();
        result.then(function (data) {
            scope.consumptions = data;
            scope.consumptions.forEach(function (con) {
                con.date = convertSvc.toDate(con.date);
                con.amt = convertSvc.ConvtEngToBang(con.amount);

            });
        }, function (e) {
            alert(e);
        });
    }


    function edit(sect) {
        scope.isEdit = true;
        scope.consumption = sect;
        //scope.consumption.date = convertSvc.toDate(sect.date);

        scope.sector.ID = sect.srcDescId;
        scope.sector.elementName = sect.sourceName;
    }

    function save() {
        scope.submitted = true;
        if (scope.consumptionForm.$valid) {
            var result = null;
            if (scope.isEdit) {
                //scope.consumption.sourceName = "";
                result = consumptionService.edit(scope.consumption);
            }
            else {
                result = consumptionService.save(scope.consumption);
            }
            result.then(function (data) {
                alert("ডাটা সেভ হয়েছে");
                initialize();
            }, function (e) {
                alert(e);
            });
        }
    }

    function deleteConsumption(sec) {
        var result = consumptionService.deleteConsumption(sec);
        result.then(function (data) {
            alert("Deleted successfully");
            initialize();
        }, function () {
            alert('ডিলিট হয়নি। ');
        });
    }

}]);