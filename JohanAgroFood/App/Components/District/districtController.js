﻿app.controller("districtCtlr", ["$scope", "districtSvc", function (scope, districtSvc) {
    scope.district = {};
    scope.submitted = false;
    scope.districts = [];
    scope.isEdit = null;

    //methods
    scope.save = save;
    scope.edit = edit;
    scope.delete = deleteDistrict;

    init();

    //To Get All Records  
    function init() {
        scope.district = {};
        scope.isEdit = false;
        getDistricts();

    }

    function getDistricts() {
        var result = districtSvc.getDistrict();
        result.then(function (data) {
            scope.districts= data;
        }, function (e) {
            alert(e);
        });
    }


    function edit(dst) {
        scope.isEdit = true;
        scope.district = dst;
    }

    function save() {
        scope.submitted = true;
        if (scope.districtForm.$valid) {
            var result = null;
            if (scope.isEdit) {
                result = districtSvc.edit(scope.district);
            }
            else {
                scope.district.elementCode = 1;
                result = districtSvc.save(scope.district);
            }
            result.then(function (data) {
                alert("ডাটা সেভ হয়েছে");
                init();
            }, function (e) {
                alert(e);
            });
        }
    }

    function deleteDistrict(dst) {
        var result = districtSvc.deleteDistrict(dst);
        result.then(function (data) {
            alert("ডাটা ডিলিট হয়েছে");
            init();
        }, function () {
            alert('ডিলিট হয়নি। ');
        });
    }

}]);