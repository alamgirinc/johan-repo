﻿using Johan.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Johan.Repository
{
    public interface IHuskSellRepository : IDisposable
    {
        List<tblSell> GetHuskInfo();

        int SaveHusk(tblSell huskInfo);

        List<STK_tblStock> GetStock();
    }
}
