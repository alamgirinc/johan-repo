﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Johan.DATA;

namespace Johan.Repository.IDAL
{
    public interface IDuePaymentRepository:IDisposable
    {
        List<tblPayable> GetDuePayment(int objPartyId);
        int Save(tblIncomeSource objDueIncomeSource);

    }
}
