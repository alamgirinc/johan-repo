﻿using Johan.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Johan.Repository
{
    public interface IRiceRepository : IDisposable
    {
        List<tblSell> GetRiceInfo();

        int SaveRice(tblSell riceInfo);

        List<STK_tblStock> GetStock();

        bool EditRiceSell(tblSell riceInfo);
        bool DeleteRiceSell(int pk);
        List<object> GetRiceInfoRpt(tblSell riceRpt);

        List<tblProduct> LoadRice(int StockId);
    }
}
