﻿using Johan.DATA;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Johan.Repository
{
    public class DRiceRepository:Disposable,IRiceRepository
    {
        private JohanAgroFoodDBEntities context = null;

        public DRiceRepository(JohanAgroFoodDBEntities context)
            : base(context)
        {

            this.context = context;    
        }
        public List<tblSell> GetRiceInfo()
        {
            var data = context.sp_getProductSellInfo(0,2);// 2 is parent for ricetype
            List<tblSell> sells = new List<tblSell>();
            foreach (var item in data)
            {
                tblSell sellobj = new tblSell();
                sellobj.ID = item.ID;
                sellobj.date = item.date;
                sellobj.noOfBag= item.noOfBag;
                sellobj.partyId= item.partyId;
                sellobj.partyName= item.partyName;
                sellobj.productId = item.productId;
                sellobj.productName = item.productName;
                sellobj.quantity= item.quantity;
                sellobj.stockId = item.stockId;
                sellobj.stockName = item.stockName;
                sellobj.unit=item.unit;
                sellobj.unitPrice = item.unitPrice;
                sells.Add(sellobj);
            }
            //List<tblLoanar> results = new List<tblLoanar>();
            //foreach (var item in loaners)
            //{
            //    results.Add(new tblParty() { ID=item.ID, name=item.name, contactNo=item.contactNo, area=item.area,
            //     district=item.district, zoneId=item.zoneId, productId=item.productId, isCashParty=item.isCashParty});
            //}
            return sells;
        }
        public List<tblProduct> LoadRice(int StockId)
        {
            var results = from rc in context.STK_Balance.Where(rr => rr.stockId == StockId)
                          join pr in context.tblProducts.Where(pp=>pp.parentId==2) on rc.productId equals pr.ID // 2 for rice
                          select pr;
            return results.Distinct().ToList();
        }

        public int SaveRice(tblSell riceInfo)
        {
            try
            {
                int maxId = context.tblSells.Select(p => p.ID).DefaultIfEmpty(0).Max();
                riceInfo.ID = ++maxId;
                if (riceInfo.paidAmount > 0)
                {
                    int incId = context.tblIncomeSources.Select(i => i.ID).DefaultIfEmpty(0).Max();
                    tblIncomeSource incomeObj = new tblIncomeSource();
                    incomeObj.ID = ++incId;
                    incomeObj.sourceName = "Rice"; // shoul be come from commonelement
                    incomeObj.srcDescId = 20; // should be come from commonelemnt
                    incomeObj.description = "income from rice sell";
                    incomeObj.amount = riceInfo.paidAmount;
                    incomeObj.date = riceInfo.date;
                    context.tblIncomeSources.Add(incomeObj);

                    riceInfo.incSrcId = incomeObj.ID;
                }
                context.tblSells.Add(riceInfo);

                //substract rice from stock
                STK_Balance riceStk = context.STK_Balance.Where(ss => ss.stockId == riceInfo.stockId && ss.productId == riceInfo.productId).FirstOrDefault();//&& ss.sackWeight==riceInfo.quantity
                riceStk.sackQuantity -= riceInfo.noOfBag;

                #region stock transaction

                long maxprdstkId = context.STK_Transaction.Select(p => p.ID).DefaultIfEmpty(0).Max();
                long laststkId = context.STK_Transaction.Where(s => s.stockId == riceInfo.stockId && s.prodId == riceInfo.productId).Select(l => l.ID).DefaultIfEmpty(0).Max();

                STK_Transaction objStkTrans = new STK_Transaction();
                objStkTrans.ID = maxprdstkId + 1;
                objStkTrans.date = riceInfo.date;
                objStkTrans.rcvQty = 0;
                objStkTrans.sellQty = riceInfo.noOfBag;
                objStkTrans.stockId = riceInfo.stockId.Value;
                objStkTrans.prodId = riceInfo.productId;
                objStkTrans.operation = 1;

                var lastTrans = context.STK_Transaction.Where(ll => ll.ID == laststkId).FirstOrDefault();
                objStkTrans.openingStock = lastTrans == null ? 0 - riceInfo.noOfBag : lastTrans.openingStock - riceInfo.noOfBag;
                context.STK_Transaction.Add(objStkTrans);
                #endregion

                return context.SaveChanges() > 0 ? riceInfo.ID : 0;
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message);
            }
        }

        public List<STK_tblStock> GetStock()
        {
            List<STK_tblStock> stks = context.STK_tblStock.ToList();
            return stks;
        }
       
        public bool EditRiceSell(tblSell riceInfo)
        {
            var orgRiceSell = context.tblSells.Where(ss => ss.ID == riceInfo.ID).FirstOrDefault();

            // edit rice stock
            STK_Balance riceStk = context.STK_Balance.Where(ss => ss.stockId == riceInfo.stockId && ss.productId == riceInfo.productId).FirstOrDefault();// && ss.sackWeight == riceInfo.quantity
            riceStk.sackQuantity += orgRiceSell.noOfBag;
            riceStk.sackQuantity -= riceInfo.noOfBag;

            // edit income source
            var orgIncomeSrc = context.tblIncomeSources.Where(ii => ii.ID == riceInfo.incSrcId).FirstOrDefault();
            if (orgIncomeSrc!=null)
            {
                orgIncomeSrc.amount = riceInfo.paidAmount;
                orgIncomeSrc.date = riceInfo.date;

            }
            else if (orgIncomeSrc==null&&riceInfo.paidAmount > 0)
            {
                int incId = context.tblIncomeSources.Select(i => i.ID).DefaultIfEmpty(0).Max();
                tblIncomeSource incomeObj = new tblIncomeSource();
                incomeObj.ID = ++incId;
                incomeObj.sourceName = "Rice"; // shoul be come from commonelement
                incomeObj.srcDescId = 20; // should be come from commonelemnt
                incomeObj.description = "income from rice sell";
                incomeObj.amount = riceInfo.paidAmount;
                incomeObj.date = riceInfo.date;
                context.tblIncomeSources.Add(incomeObj);

                orgRiceSell.incSrcId = incomeObj.ID;
            }

            #region edit stock transaction

            long maxprdstkId = context.STK_Transaction.Select(p => p.ID).DefaultIfEmpty(0).Max();
            long laststkId = context.STK_Transaction.Where(s => s.stockId == riceInfo.stockId && s.prodId == riceInfo.productId).Select(l => l.ID).DefaultIfEmpty(0).Max();

            STK_Transaction objStkTrans = new STK_Transaction();
            objStkTrans.ID = maxprdstkId + 1;
            objStkTrans.date = riceInfo.date;
            objStkTrans.rcvQty = 0;
            objStkTrans.sellQty = riceInfo.noOfBag;
            objStkTrans.stockId = riceInfo.stockId.Value;
            objStkTrans.prodId = riceInfo.productId;
            objStkTrans.operation = 2;
            var lastTrans = context.STK_Transaction.Where(ll => ll.ID == laststkId).FirstOrDefault();
            objStkTrans.openingStock = lastTrans == null ? orgRiceSell.noOfBag - riceInfo.noOfBag : lastTrans.openingStock + orgRiceSell.noOfBag - riceInfo.noOfBag;
            context.STK_Transaction.Add(objStkTrans);
            #endregion

            // edit tblsell
            orgRiceSell.productId = riceInfo.productId;
            orgRiceSell.productName = riceInfo.productName;
            orgRiceSell.noOfBag = riceInfo.noOfBag;
            orgRiceSell.paidAmount = riceInfo.paidAmount;
            orgRiceSell.partyId = riceInfo.partyId;
            orgRiceSell.partyName = riceInfo.partyName;
            orgRiceSell.quantity = riceInfo.quantity;
            orgRiceSell.stockId = riceInfo.stockId;
            orgRiceSell.stockName = riceInfo.stockName;
            orgRiceSell.unit = riceInfo.unit;
            orgRiceSell.unitPrice = riceInfo.unitPrice;


            return context.SaveChanges() > 0;
        }

        public bool DeleteRiceSell(int pk)
        {
            var orgRiceSell = context.tblSells.Where(ss => ss.ID == pk).FirstOrDefault();

            #region edit stock transaction     
            long maxprdstkId = context.STK_Transaction.Select(p => p.ID).DefaultIfEmpty(0).Max();
            long laststkId = context.STK_Transaction.Where(s => s.stockId == orgRiceSell.stockId && s.prodId == orgRiceSell.productId).Select(l => l.ID).DefaultIfEmpty(0).Max();

            STK_Transaction objStkTrans = new STK_Transaction();
            objStkTrans.ID = maxprdstkId + 1;
            objStkTrans.date = DateTime.Now;
            objStkTrans.rcvQty = 0;
            objStkTrans.sellQty = 0;
            objStkTrans.stockId = orgRiceSell.stockId.Value;
            objStkTrans.prodId = orgRiceSell.productId;
            objStkTrans.operation = 3;
            var lastTrans = context.STK_Transaction.Where(ll => ll.ID == laststkId).FirstOrDefault();
            objStkTrans.openingStock = lastTrans == null ? orgRiceSell.noOfBag : lastTrans.openingStock + orgRiceSell.noOfBag;
            context.STK_Transaction.Add(objStkTrans);
            #endregion

            // edit rice stock
            STK_Balance riceStk = context.STK_Balance.Where(ss => ss.stockId == orgRiceSell.stockId && ss.productId == orgRiceSell.productId).FirstOrDefault();//&& ss.sackWeight == riceInfo.quantity
            riceStk.sackQuantity += orgRiceSell.noOfBag;

            // delete income source
            var orgIncomeSrc = context.tblIncomeSources.Where(ii => ii.ID == orgRiceSell.incSrcId).FirstOrDefault();
            if (orgIncomeSrc != null)
            {
                context.tblIncomeSources.Remove(orgIncomeSrc);
            }
            //delete tblsell
            context.tblSells.Remove(orgRiceSell);

            return context.SaveChanges() > 0;
        }
        public List<object> GetRiceInfoRpt(tblSell riceRpt)
        {
            int parentId = 2;// 2 for rice
            var riceInfoLst = context.sp_GetProductInfo(riceRpt.partyId,parentId, riceRpt.fromDate, riceRpt.toDate);
            return riceInfoLst.ToList<object>();
        }
    }
}