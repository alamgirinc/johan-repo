﻿using Johan.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Johan.Repository
{
    public class DPartyRepository:Disposable,IPartyRepository
    {
        private JohanAgroFoodDBEntities context = null;

        public DPartyRepository(JohanAgroFoodDBEntities context)
            : base(context)
        {
            this.context = context;    
        }
        public List<tblParty> GetParty()
        {
            List<tblParty> partys = context.tblParties.ToList();
            List<tblParty> results = new List<tblParty>();
            foreach (var item in partys)
            {
                results.Add(new tblParty() { ID=item.ID, name=item.name, contactNo=item.contactNo, area=item.area,
                 district=item.district, zoneId=item.zoneId, productId=item.productId, isCashParty=item.isCashParty});
            }
            return results;
        }

        public bool SaveParty(tblParty party)
        {
            context.tblParties.Add(party);
            return context.SaveChanges() > 0;
        }
        public bool EditParty(tblParty objEditParty)
        {
            var orgEditParty = context.tblParties.Where(ss => ss.ID == objEditParty.ID).FirstOrDefault();
            orgEditParty.name = objEditParty.name;
            orgEditParty.contactNo = objEditParty.contactNo;
            orgEditParty.area = objEditParty.area;
            orgEditParty.zoneId = objEditParty.zoneId;
            orgEditParty.district = objEditParty.district;
            orgEditParty.productId = objEditParty.productId;
            orgEditParty.isCashParty = objEditParty.isCashParty;

            return context.SaveChanges() > 0;
        }
        public bool Delete(int pk)
        {
            var orgDeleteParty = context.tblParties.Where(ss => ss.ID == pk).FirstOrDefault();


            context.tblParties.Remove(orgDeleteParty);
            return context.SaveChanges() > 0;
        }
    }
}
