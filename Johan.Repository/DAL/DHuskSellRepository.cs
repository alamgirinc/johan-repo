﻿using Johan.DATA;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Johan.Repository
{
    public class DHuskSellRepository:Disposable,IHuskSellRepository
    {
        private JohanAgroFoodDBEntities context = null;

        public DHuskSellRepository(JohanAgroFoodDBEntities context)
            : base(context)
        {

            this.context = context;    
        }
        public List<tblSell> GetHuskInfo()
        {
            var data = context.sp_getProductSellInfo(4,0);// 4 is id of husk
            List<tblSell> sells = new List<tblSell>();
            foreach (var item in data)
            {
                tblSell sellobj = new tblSell();
                sellobj.ID = item.ID;
                sellobj.date = item.date;
                sellobj.noOfBag= item.noOfBag;
                sellobj.partyId= item.partyId;
                sellobj.partyName= item.partyName;
                sellobj.productId = item.productId;
                sellobj.productName = item.productName;
                sellobj.quantity= item.quantity;
                //sellobj.stockName = item.stockName;
                sellobj.unit=item.unit;
                sellobj.unitPrice = item.unitPrice;
                sells.Add(sellobj);
            }
            //List<tblLoanar> results = new List<tblLoanar>();
            //foreach (var item in loaners)
            //{
            //    results.Add(new tblParty() { ID=item.ID, name=item.name, contactNo=item.contactNo, area=item.area,
            //     district=item.district, zoneId=item.zoneId, productId=item.productId, isCashParty=item.isCashParty});
            //}
            return sells;
        }

        public int SaveHusk(tblSell huskInfo)
        {
            int maxId = context.tblSells.Select(p => p.ID).DefaultIfEmpty(0).Max();
            huskInfo.ID = ++maxId;
            if (huskInfo.paidAmount>0)
            {
                int incId = context.tblIncomeSources.Select(i => i.ID).DefaultIfEmpty(0).Max();
                tblIncomeSource incomeObj = new tblIncomeSource();
                incomeObj.ID = ++incId;
                incomeObj.sourceName = "Husk"; // shoul be come from commonelement
                incomeObj.srcDescId = 21; // should be come from commonelemnt
                incomeObj.description = "income from husk sell";
                incomeObj.amount = huskInfo.paidAmount;
                incomeObj.date = huskInfo.date;
                context.tblIncomeSources.Add(incomeObj);
            }
            context.tblSells.Add(huskInfo);

            //substract rice from stock
            STK_Balance riceStk = context.STK_Balance.Where(ss => ss.stockId == huskInfo.stockId && ss.productId == huskInfo.productId).FirstOrDefault();//&& ss.sackWeight==riceInfo.quantity
            riceStk.sackQuantity -= huskInfo.noOfBag;
            //tblSell retRice = new tblSell();
            //retRice.ID = riceInfo.ID;
            return context.SaveChanges() > 0 ? huskInfo.ID : 0;
        }

        public List<STK_tblStock> GetStock()
        {
            List<STK_tblStock> stks = context.STK_tblStock.ToList();
            return stks;
        }


    }
}