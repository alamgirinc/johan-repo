﻿using Johan.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Johan.Repository
{
    public class DBalanceRepository:Disposable,IBalanceRepository
    {    
        private JohanAgroFoodDBEntities context = null;

        public DBalanceRepository(JohanAgroFoodDBEntities context)
            : base(context)
        {

            this.context = context;    
        }

        public List<tblPayable> GetBalanceInfo()
        {
            var results = from pay in context.tblPayables
                          join pr in context.tblParties on pay.partyId equals pr.ID
                          select new 
                          {
                              pay.ID,
                              pay.partyId,
                              pr.name,
                              pay.amount,
                              pay.date
                          };
            List<tblPayable> payableList = new List<tblPayable>();
            foreach (var item in results)
            {
                tblPayable obj = new tblPayable();
                obj.ID = item.ID;
                obj.partyId = item.partyId;
                obj.partyName = item.name;
                obj.amount = item.amount;
                obj.date = item.date;
                payableList.Add(obj);
            }
            return payableList;
        }


        public int SaveBalance(tblPayable objPayable)
        {
            try
            {
                int maxId = context.tblPayables.Select(p => p.ID).DefaultIfEmpty(0).Max();
                objPayable.ID = ++maxId;
                var lastPartyId =
                    context.tblPayables.Where(p => p.partyId == objPayable.partyId)
                        .Select(p => p.ID)
                        .DefaultIfEmpty()
                        .Max();
                context.tblPayables.Where(p => p.ID == lastPartyId).FirstOrDefault().isActive = 0;
                objPayable.isActive = 1;
                objPayable.openingBalance = Convert.ToInt32(objPayable.amount) + context.tblPayables.Where(p => p.ID == lastPartyId).FirstOrDefault().openingBalance;
                context.tblPayables.Add(objPayable);
                return context.SaveChanges() > 0 ? objPayable.ID : 0;
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message);
            }
        }
        public bool Edit(tblPayable objPayable)
        {
            var orgEditBalanceInfo = context.tblPayables.Where(ss => ss.ID == objPayable.ID).FirstOrDefault();
            orgEditBalanceInfo.partyName = objPayable.partyName;
            orgEditBalanceInfo.date = objPayable.date;
            orgEditBalanceInfo.amount = objPayable.amount;

            return context.SaveChanges() > 0;
        }

        public bool Delete(int pk)
        {
            var orgDeleteBalanceInfo = context.tblPayables.Where(ss => ss.ID == pk).FirstOrDefault();
            context.tblPayables.Remove(orgDeleteBalanceInfo);
            return context.SaveChanges() > 0;
        }
    }
}
