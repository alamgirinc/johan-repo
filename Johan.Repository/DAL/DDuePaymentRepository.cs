﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Johan.DATA;
using Johan.Repository.IDAL;

namespace Johan.Repository.DAL
{
    public class DDuePaymentRepository : Disposable, IDuePaymentRepository
    {
        private JohanAgroFoodDBEntities context = null;
        public DDuePaymentRepository(JohanAgroFoodDBEntities context)
            : base(context)
        {
            this.context = context;
        }

        public List<tblPayable> GetDuePayment(int objPartyId)
        {
            var results = from pay in context.tblPayables
                          join pr in context.tblParties on pay.partyId equals pr.ID
                          where pay.partyId == objPartyId
                          select new
                          {
                              pay.ID,
                              pay.partyId,
                              pr.name,
                              pay.amount,
                              pay.date
                          };
            List<tblPayable> payableList = new List<tblPayable>();
            foreach (var item in results)
            {
                tblPayable obj = new tblPayable();
                obj.ID = item.ID;
                obj.partyId = item.partyId;
                obj.partyName = item.name;
                obj.amount = item.amount;
                obj.date = item.date;
                payableList.Add(obj);
            }
            return payableList;
        }

        public int Save(tblIncomeSource objDueIncomeSource)
        {
            try
            {
                int maxId = context.tblIncomeSources.Select(ss => ss.ID).DefaultIfEmpty(0).Max();
                objDueIncomeSource.ID = ++maxId;
                context.tblIncomeSources.Add(objDueIncomeSource);

                tblPayable payableChange =
                    context.tblPayables.Where(ss => ss.partyId == objDueIncomeSource.partyId).FirstOrDefault();
                payableChange.amount -= objDueIncomeSource.amount;

                return context.SaveChanges() > 0 ? objDueIncomeSource.ID : 0;
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message);
            }
        }
    }
}
