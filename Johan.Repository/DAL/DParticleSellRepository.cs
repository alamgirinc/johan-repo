﻿using Johan.DATA;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Johan.Repository
{
    public class DParticleSellRepository:Disposable,IParticleSellRepository
    {
        private JohanAgroFoodDBEntities context = null;

        public DParticleSellRepository(JohanAgroFoodDBEntities context)
            : base(context)
        {

            this.context = context;    
        }
        public List<tblSell> GetParticleInfo()
        {
            var data = context.sp_getProductSellInfo(0,11);// 11 is id of Particle
            List<tblSell> sells = new List<tblSell>();
            foreach (var item in data)
            {
                tblSell sellobj = new tblSell();
                sellobj.ID = item.ID;
                sellobj.date = item.date;
                sellobj.noOfBag= item.noOfBag;
                sellobj.partyId= item.partyId;
                sellobj.partyName= item.partyName;
                sellobj.productId = item.productId;
                sellobj.productName = item.productName;
                sellobj.quantity= item.quantity;
                sellobj.stockId = item.stockId;
                sellobj.stockName = item.stockName;
                sellobj.unit=item.unit;
                sellobj.unitPrice = item.unitPrice;
                sells.Add(sellobj);
            }
            return sells;
        }

        public int SaveParticle(tblSell particleInfo)
        {   
            int maxId = context.tblSells.Select(p => p.ID).DefaultIfEmpty(0).Max();
            particleInfo.ID = ++maxId;
            if (particleInfo.paidAmount > 0)
            {
                int incId = context.tblIncomeSources.Select(i => i.ID).DefaultIfEmpty(0).Max();
                tblIncomeSource incomeObj = new tblIncomeSource();
                incomeObj.ID = ++incId;
                incomeObj.sourceName = "Particle"; // shoul be come from commonelement
                incomeObj.srcDescId = 21; // should be come from commonelemnt
                incomeObj.description = "income from Particle sell";
                incomeObj.amount = particleInfo.paidAmount;
                incomeObj.date = particleInfo.date;
                context.tblIncomeSources.Add(incomeObj);

                particleInfo.incSrcId = incomeObj.ID;
            }
            context.tblSells.Add(particleInfo);

            //substract particle from stock
            STK_Balance particleStk = context.STK_Balance.Where(ss => ss.stockId == particleInfo.stockId && ss.productId == particleInfo.productId).FirstOrDefault();//&& ss.sackWeight==particleInfo.quantity
            particleStk.sackQuantity -= particleInfo.noOfBag;

            #region stock transaction

            long maxprdstkId = context.STK_Transaction.Select(p => p.ID).DefaultIfEmpty(0).Max();
            long laststkId = context.STK_Transaction.Where(s => s.stockId == particleInfo.stockId && s.prodId == particleInfo.productId).Select(l => l.ID).DefaultIfEmpty(0).Max();

            STK_Transaction objStkTrans = new STK_Transaction();
            objStkTrans.ID = maxprdstkId + 1;
            objStkTrans.date = particleInfo.date;
            objStkTrans.rcvQty = 0;
            objStkTrans.sellQty = particleInfo.noOfBag;
            objStkTrans.stockId = particleInfo.stockId.Value;
            objStkTrans.prodId = particleInfo.productId;
            objStkTrans.operation = 1;

            var lastTrans = context.STK_Transaction.Where(ll => ll.ID == laststkId).FirstOrDefault();
            objStkTrans.openingStock = lastTrans == null ? 0 - particleInfo.noOfBag : lastTrans.openingStock - particleInfo.noOfBag;
            context.STK_Transaction.Add(objStkTrans);
            #endregion

            return context.SaveChanges() > 0 ? particleInfo.ID : 0;
        }

        public bool EditParticleSell(tblSell particleInfo)
        {
            try
            {
            var orgParticleSell = context.tblSells.Where(ss => ss.ID == particleInfo.ID).FirstOrDefault();

            // edit particle stock
            STK_Balance particleStk = context.STK_Balance.Where(ss => ss.stockId == particleInfo.stockId && ss.productId == particleInfo.productId).FirstOrDefault();// && ss.sackWeight == particleInfo.quantity
            particleStk.sackQuantity += orgParticleSell.noOfBag;
            particleStk.sackQuantity -= particleInfo.noOfBag;

            // edit income source
            var orgIncomeSrc = context.tblIncomeSources.Where(ii => ii.ID == particleInfo.incSrcId).FirstOrDefault();
            if (orgIncomeSrc != null)
            {
                orgIncomeSrc.amount = particleInfo.paidAmount;
                orgIncomeSrc.date = particleInfo.date;

            }
            else if (orgIncomeSrc == null && particleInfo.paidAmount > 0)
            {
                int incId = context.tblIncomeSources.Select(i => i.ID).DefaultIfEmpty(0).Max();
                tblIncomeSource incomeObj = new tblIncomeSource();
                incomeObj.ID = ++incId;
                incomeObj.sourceName = "Particle"; // shoul be come from commonelement
                incomeObj.srcDescId = 20; // should be come from commonelemnt
                incomeObj.description = "income from particle sell";
                incomeObj.amount = particleInfo.paidAmount;
                incomeObj.date = particleInfo.date;
                context.tblIncomeSources.Add(incomeObj);

                orgParticleSell.incSrcId = incomeObj.ID;
            }

            #region edit stock transaction

            long maxprdstkId = context.STK_Transaction.Select(p => p.ID).DefaultIfEmpty(0).Max();
            long laststkId = context.STK_Transaction.Where(s => s.stockId == particleInfo.stockId && s.prodId == particleInfo.productId).Select(l => l.ID).DefaultIfEmpty(0).Max();

            STK_Transaction objStkTrans = new STK_Transaction();
            objStkTrans.ID = maxprdstkId + 1;
            objStkTrans.date = particleInfo.date;
            objStkTrans.rcvQty = 0;
            objStkTrans.sellQty = particleInfo.noOfBag;
            objStkTrans.stockId = particleInfo.stockId.Value;
            objStkTrans.prodId = particleInfo.productId;
            objStkTrans.operation = 2;
            var lastTrans = context.STK_Transaction.Where(ll => ll.ID == laststkId).FirstOrDefault();
            objStkTrans.openingStock = lastTrans == null ? orgParticleSell.noOfBag - particleInfo.noOfBag : lastTrans.openingStock + orgParticleSell.noOfBag - particleInfo.noOfBag;
            context.STK_Transaction.Add(objStkTrans);
            #endregion

            // edit tblsell
            orgParticleSell.productId = particleInfo.productId;
            orgParticleSell.productName = particleInfo.productName;
            orgParticleSell.noOfBag = particleInfo.noOfBag;
            orgParticleSell.paidAmount = particleInfo.paidAmount;
            orgParticleSell.partyId = particleInfo.partyId;
            orgParticleSell.partyName = particleInfo.partyName;
            orgParticleSell.quantity = particleInfo.quantity;
            orgParticleSell.stockId = particleInfo.stockId;
            orgParticleSell.stockName = particleInfo.stockName;
            orgParticleSell.unit = particleInfo.unit;
            orgParticleSell.unitPrice = particleInfo.unitPrice;

            return context.SaveChanges() > 0;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool DeleteParticleSell(int pk)
        {
            var orgParticleSell = context.tblSells.Where(ss => ss.ID == pk).FirstOrDefault();

            #region edit stock transaction
            long maxprdstkId = context.STK_Transaction.Select(p => p.ID).DefaultIfEmpty(0).Max();
            long laststkId = context.STK_Transaction.Where(s => s.stockId == orgParticleSell.stockId && s.prodId == orgParticleSell.productId).Select(l => l.ID).DefaultIfEmpty(0).Max();

            STK_Transaction objStkTrans = new STK_Transaction();
            objStkTrans.ID = maxprdstkId + 1;
            objStkTrans.date = DateTime.Now;
            objStkTrans.rcvQty = 0;
            objStkTrans.sellQty = 0;
            objStkTrans.stockId = orgParticleSell.stockId.Value;
            objStkTrans.prodId = orgParticleSell.productId;
            objStkTrans.operation = 3;
            var lastTrans = context.STK_Transaction.Where(ll => ll.ID == laststkId).FirstOrDefault();
            objStkTrans.openingStock = lastTrans == null ? orgParticleSell.noOfBag : lastTrans.openingStock + orgParticleSell.noOfBag;
            context.STK_Transaction.Add(objStkTrans);
            #endregion

            // edit particle stock
            STK_Balance particleStk = context.STK_Balance.Where(ss => ss.stockId == orgParticleSell.stockId && ss.productId == orgParticleSell.productId).FirstOrDefault();//&& ss.sackWeight == particleInfo.quantity
            particleStk.sackQuantity += orgParticleSell.noOfBag;

            // delete income source
            var orgIncomeSrc = context.tblIncomeSources.Where(ii => ii.ID == orgParticleSell.incSrcId).FirstOrDefault();
            if (orgIncomeSrc != null)
            {
                context.tblIncomeSources.Remove(orgIncomeSrc);
            }
            //delete tblsell
            context.tblSells.Remove(orgParticleSell);

            return context.SaveChanges() > 0;
        }
        public List<object> GetParticleInfoRpt(tblSell particleRpt)
        {
            int parentId = 11;//particle id
            var particleInfoLst = context.sp_GetProductInfo(particleRpt.partyId,parentId, particleRpt.fromDate, particleRpt.toDate);
            return particleInfoLst.ToList<object>();
        }


        public List<STK_tblStock> GetStock()
        {
            List<STK_tblStock> stks = context.STK_tblStock.ToList();
            return stks;
        }

        public bool SaveParticleStock(STK_Balance particleStk)
        {
            int maxId = context.STK_Balance.Select(p => p.ID).DefaultIfEmpty(0).Max();
            particleStk.ID = ++maxId;
            context.STK_Balance.Add(particleStk);
            return context.SaveChanges() > 0;
        }

        public List<STK_Balance> GetParticleStock()
        {
            var particlestks = context.STK_Balance.Join(context.tblProducts,
                    particle => particle.productId,
                    prod => prod.ID,
                    (particle, prod) => new
                    {
                        ID = particle.ID,
                        productId = particle.productId,
                        sackQuantity = particle.sackQuantity,
                        stockId = particle.stockId,
                        productName = prod.productName
                    }).Join(context.STK_tblStock, particlep => particlep.stockId, stk => stk.ID,
                    (particlep, stk) => new
                    {
                        ID = particlep.ID,
                        productId = particlep.productId,
                        sackQuantity = particlep.sackQuantity,
                        stockId = particlep.stockId,
                        productName = particlep.productName,
                        stockName = stk.stockName
                    }).ToList();
            List<STK_Balance> rLst = new List<STK_Balance>();

            foreach (var x in particlestks)
            {
                STK_Balance objR = new STK_Balance();

                objR.ID = x.ID;
                objR.productId = x.productId;
                objR.sackQuantity = x.sackQuantity;
                objR.stockId = x.stockId;
                objR.productName = x.productName;
                objR.stockName = x.stockName;
                rLst.Add(objR);
            }
            return rLst;
        }

        public bool DeleteParticleStock(STK_Balance particleStk)
        {
            var orgStk = context.STK_Balance.Where(ss => ss.ID == particleStk.ID).FirstOrDefault();
            context.STK_Balance.Remove(orgStk);
            return context.SaveChanges() > 0;
        }

        public bool EditParticleStock(STK_Balance particleStk)
        {
            var orgStk = context.STK_Balance.Where(ss => ss.ID == particleStk.ID).FirstOrDefault();
            orgStk.productId = particleStk.productId;
            orgStk.sackQuantity = particleStk.sackQuantity;
            orgStk.stockId = particleStk.stockId;

            return context.SaveChanges() > 0;
        }
    }
}