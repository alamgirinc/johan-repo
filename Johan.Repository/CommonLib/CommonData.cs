﻿using Johan.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Johan.Repository
{
    public static class CommonData
    {
        public static List<tblProduct> GetAllProduct(JohanAgroFoodDBEntities context) 
        {
            return context.tblProducts.ToList();
        }

    }
}
